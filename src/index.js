const express = require("express");
const morgan = require("morgan");
const router = require("./router/router");
const app = express();
const PORT = 3000;

app.set("view engine", "ejs");
app.set("views", "./src/views");
//morgan
app.use(morgan("common"));

//folder config
app.use(express.static("./src/public/assets"));

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(router);

app.listen(PORT, () => {
  console.log(`web berjalan pada port ${PORT}`);
});
